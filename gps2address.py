#! -*- coding: utf-8 -*-
import requests, re
from urllib import parse

class mapTools(object):
    def __init__(self):
        # 获取key
        self._homepage = 'http://lbs.amap.com/console/show/picker'
        self._headers = {
            'DNT': '1',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'zh-CN,zh;q=0.8',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36',
            'Accept': '*/*',
            'Referer': self._homepage,
        }
		
        self._s = requests.session()
        self._r = self._s.get(self._homepage, headers=self._headers)
        self._r.encoding = 'gbk2312'
        self._key = re.findall(r'key=(.*?)"></script>', self._r.text)[0]

    def GPS2Address(self, gps_lat, gps_lon):
        # 转换地址
        self._bashurl = 'http://restapi.amap.com/v3/geocode/regeo?'
        self._data = {
            'key':self._key,
            's':'rsv3',
            'location': '%s,%s' %(gps_lon, gps_lat),
            'radius':'2800',
            'platform':'JS',
            'logversion':'2.0',
            'sdkversion':'1.3',
            'appname':self._homepage,
        }
        self._bashurl = self._bashurl + parse.urlencode(self._data)
        self._result = self._s.get(self._bashurl, headers=self._headers)
        return eval(self._result.text)


if __name__ == '__main__':
    # 通过高德地图坐标拾取, 把GPS坐标转换为可读地址   http://lbs.amap.com/console/show/picker
    gps_lat = '39.9097239'    # 纬度
    gps_lon = '116.39795'     # 经度
    map = mapTools()
    result = map.GPS2Address(gps_lat, gps_lon)
    print(result)