#-*- coding:utf-8 -*-

from threading import Thread,Lock
from queue import Queue
from time import sleep, time

q = Queue()	# 任务队列
Num = 4		# 并发线程数
Jobs = 100	# 任务数量
lock = Lock()	# 线程锁

# 具体的处理函数， 负责处理单个任务
def do_somthing_using(ThreadID, arguments):
	# lock.acquire()
	print(ThreadID, ': ',arguments)
	# lock.release()

# 这是个工作进程, 负责不断从队列中取数据并处理
def working(ThreadID):
	while True:
		arguments = q.get()
		do_somthing_using(ThreadID, arguments)
		sleep(1)
		q.task_done()

# 开启线程
threads = []
for i in range(Num):
	t = Thread(target=working, args=('Thread-%s'%i,))	# 线程的执行函数为working
	threads.append(t)
for item in threads:
	item.setDaemon(True)
	item.start()

# Jobs入队
for i in range(Jobs):
	q.put(i)

# 等待所有队列为空
q.join()