#-*- coding:utf-8 -*-
from dns import resolver  
# pip install dnspython 


class QueryDNSResolver(object):
	'''
	DNS解析， A类， MX， NS， CNAME
	'''
	def __init__(self, DNSServer = None):
		self.resolver = resolver.Resolver()
		# 自定义DNSserver
		if DNSServer:
			self.resolver.nameservers = [DNSServer]
	
	def A_query(self, domain):
		list = []
		answer = self.resolver.query(domain)
		for i in answer.response.answer:
			for j in i.items:
				try: list.append(j.address)
				except: pass
		return list
if __name__ == '__main__':
	d = QueryDNSResolver('1.1.1.1')
	print(d.A_query('www.example.com'))