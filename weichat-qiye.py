# -*- coding: utf-8 -*-
import requests

class Weichat(object):
    def __init__(self, CorpID, Secret):
        self.__CorpID = CorpID
        self.__Secret = Secret

    def get(self,Timeout = 15):
        baseurl = 'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=%s&corpsecret=%s'%(self.__CorpID, self.__Secret)
        print(baseurl)
        _web = requests.get(baseurl, timeout=Timeout, verify=False)
        # _web.text 示例
        '''
        {"errcode":0,"errmsg":"ok","access_token":"2C107doI-_z0wLv1bs...1REf-L7kabU","expires_in":7200}
        '''
        _result = _web.json()
        return _result

    def send(self, access_token, touser, agentid, content, Timeout = 15):
        baseurl = 'https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=%s' % access_token
        data = '''
            {
        	"touser": "%s",
        	"msgtype": "text",
        	"agentid": %s,
        	"text": {
        		"content": "%s"
        	}
        }
            ''' % (touser, agentid, content.encode('utf-8').decode('latin1'))
        _web = requests.post(baseurl, data, timeout=Timeout, verify=False)
        # _web.text 示例
        '''{"errcode":0,"errmsg":"ok","invaliduser":""}'''
        _result = _web.json()
        return _result

if __name__ == '__main__':
    CorpID = 'wxxxxxxxxxxx'
    Secret = '5xxxxxxxxxxxxxxxxxxxx'
    touser = 'id_1'
    agentid = 7
    content = '测试!@###%^&*())+_=-,./<>?:";\'{}[]\|'
    weichat = Weichat(CorpID, Secret)
    gettoken = weichat.get()
    access_token = gettoken['access_token']
    weichat.send(access_token, touser, agentid, content)
