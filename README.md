# script-py

一些Python代码片段.

### dns.py
```
dns 查询, 可自定义dns
```

### e-mail.py
```
e-mail 电子邮件发送,   测试zoho 
```

### gps2address.py
```
通过高德地图坐标拾取, 将GPS坐标转换为 国家省市区...  地理位置.
```

### kauidi-api.py
```
使用快递鸟查询接口
```

### multithread.py
```
多线程
```

### weichat-qiye.py
```
微信企业号 文本消息发送
```