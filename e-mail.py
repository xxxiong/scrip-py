# -*- coding: utf-8 -*-
import smtplib, socket
from email.header import Header
from email.mime.text import MIMEText
from email.utils import parseaddr, formataddr
        
class Email(object):
    def __init__(self, from_addr, from_name, password, smtp_server, smtp_port):
        self.__from_addr = from_addr
        self.__from_name = from_name
        self.__password = password
        self.__smtp_server = smtp_server
        self.__smtp_port = smtp_port


    def send(self,to_addrs, title, text):
        def __format_addr(s):
            name, addr = parseaddr(s)
            return formataddr((Header(name, 'utf-8').encode(), addr))

        body = '%s'%text
        msg = MIMEText(body, 'plain', 'utf-8')
        msg['subject'] = title
        msg['from'] = __format_addr('%s <%s>' %(self.__from_name, self.__from_addr))
        msg['to'] = ",".join(to_addrs)
        s = smtplib.SMTP_SSL(self.__smtp_server, self.__smtp_port)
        s.set_debuglevel(1)
        s.login(self.__from_addr,self.__password)
        s.sendmail(self.__from_addr, to_addrs, msg.as_string())
        s.quit()

if __name__ == '__main__':
	from_addr = 'server@zoho.cn'
	from_name = '发件人'
	password = '123456'
	smtp_server = 'smtp.zoho.cn'
	smtp_port = 465
	to_addrs = ['x@cn.com']
	title = 'title'
	content = 'content'

	e = Email(from_addr, from_name, password, smtp_server, smtp_port)
	e.send(to_addrs, title, text)